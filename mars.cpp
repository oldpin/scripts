#include <iostream>
#include<cmath>


int main(){

    float age_in_years, age_in_months, age_in_days, mars_year;


    std::cout << "Enter your age:";
    std::cin >> age_in_years;
    std::cout << "Enter your month:";
    std::cin >> age_in_months;

    // convert age and month in accurate age in days
    age_in_days = age_in_years * 365.25;
    age_in_days += age_in_months * 30;

    // calculate it in mars year
    mars_year = age_in_days / 687;
    

    // display the result
    std::cout<< round(mars_year) << " mars year!" << std::endl;

}